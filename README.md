# Python for VFX

## Snippets for Houdini

* houdini/change-file-paths/changefilepaths.py - This code will run through selected nodes and replace the specified file paths with a new one. 

## Scripts and Gizmos for Nuke

* nuke/Gizmos/ndCamDistort.gizmo

* nuke/Gizmos/pDistance.gizmo

* nuke/Python/group_colorcorrect_knobs.py - This code will create colorcorrect knobs for all relevant nodes in the selected group.