import hou

for node in hou.selectedNodes():
    for attr in node.recursiveGlob("*"): 
        params = attr.parms()
        for param in params:
            pName = param.name()

            if pName == "file":
                pContent = param.eval()

                ## So far we are just looking at the files, not changing anything, 
				## uncomment the rest after editing the paths to your satisfaction.

                pStripped = pContent.replace('I:/Users/biXen/Documents/FXPHD_TRAINING/HOU213-class02-files/FurnitureObjFiles/','I:/Courses/fxphd - HOU213/hou213-files02-pt1/FurnitureObjFiles/')
                print pStripped
                param.set(pStripped)
