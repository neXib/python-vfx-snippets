import nuke

from __future__ import with_statement

selectedNode = nuke.selectedNode()
with selectedNode:
    for n in nuke.allNodes('ColorCorrect'):        
        k = n.knob('gain')
        selectedNode.addKnob(k)        
        myLabel = n.name()
        k.setLabel(myLabel)
        k.setTooltip('Adjust gain')
selectedNode.showControlPanel()