#My custom gizmos
# nuke.menu( 'Nodes' ).addCommand( 'Filter/myGlow', lambda: nuke.createNode( 'myGlow' ) )

#My custom formats
nuke.addFormat ('1280 720 1.0 720p')

#Nuke defaults
nuke.knobDefault('Root.format', '720p')
nuke.knobDefault('Root.fps', '25')